<?php

namespace App\Http\Requests\tintuc;

use Illuminate\Foundation\Http\FormRequest;

class CreateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'tieude' => 'required',
            'noidung' => 'required'

        ];
    }
    public function messages() : array
    {
        return [
            'tieude.required' => 'Vui lòng nhập tên Tiêu Đề',
            'noidung.required' => 'Vui lòng nhập Nội Dung'
        ];
    }
}
