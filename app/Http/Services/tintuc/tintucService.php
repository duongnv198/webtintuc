<?php

namespace App\Http\Services\tintuc;

use Illuminate\Support\Facades\Session;
use App\Models\tintuc;
use App\Models\theloai;
use Illuminate\Support\Facades\Storage;


class tintucService
{
    public function getid_theloai()
    {
        return theloai::all();
    }

    public function tenTheLoai()
    {
        return theloai::whereNull('deleted_at')->pluck('tentheloai', 'id')->toArray();
    }

    // public function gettintuc()
    // {
    //     return tintuc::orderbyDesc('id')->paginate(20);
    // }


    public function create($request)
    {
        try {
            tintuc::create([
                'tieude' => (string) $request -> input('tieude'),
                'tomtat' => (string) $request -> input('tomtat'),
                'noidung' => (string) $request -> input('noidung'),
                'id_theloai' => (int) $request -> input('id_theloai'),
                'noibat' => (int) $request -> input('noibat'),
                'hinh' => (string) $request -> input('hinh')
            ]);
            Session::flash ('success', 'Tạo Tin Tức Thành Công');

        } catch (\Exception $err) {
            Session::flash('error', $err->getMessage());
            return false;
        }
        return true;
    }
    
    public function destroy($request)
    {
        $id = (int) $request -> input('id');
        $tintuc = tintuc::where('id',$request->input('id'))->first();
        if ($tintuc){
            $path = str_replace('storage','public', $tintuc->hinh);
            Storage::delete($path);
            $tintuc->delete();
            return true;
        }
        return false;
    }

    public function update($request, $tintuc) :bool
    {
        $tintuc->tieude = (string) $request-> input('tieude');
        $tintuc->tomtat = (string) $request-> input('tomtat');
        $tintuc->noidung = (string) $request-> input('noidung');
        $tintuc->hinh = (string) $request-> input('hinh');
        $tintuc->noibat = (string) $request-> input('noibat');
        $tintuc->id_theloai = (string) $request-> input('id_theloai');
        $tintuc->save();
        Session::flash('success', 'Cập Nhật Tin Tức Thành Công');
        return true;
    }
    public static function noibat($noibat = 0) : string
    {
        return $noibat ==0 ? '<span class="btn btn-danger btn-sm"> NO </span>'
         :'<span class="btn btn-success btn-sm"> YES </span>';
    }
    const LIMIT = 15;
    public function get()
    {
        return tintuc::select('id', 'tieude', 'tomtat','noidung', 'hinh','noibat', 'created_at')
        -> orderByDesc('created_at')
        -> limit(self::LIMIT)
        -> get();
    }

    public function show($id)
    {
        return tintuc::where('id', $id)
        ->where('active',1)
        ->with('menu')
        ->firtOrFail();
    }
}