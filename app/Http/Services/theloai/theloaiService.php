<?php

namespace App\Http\Services\theloai;

use Illuminate\Support\Facades\Session;
use App\Models\theloai;

class theloaiService
{
    // public function getParent()
    // {
    //     return theloai::where('parent_id',0) -> get();
    // }
    public function gettheloai1()
    {
        return theloai::where('id')->get();
    }
    
    public function gettheloai()
    {
        return theloai::orderbyDesc('id')->paginate(20);
    }

    public function create($request)
    {

        try {
            theloai::create([
                'tentheloai' => (string) $request -> input('tentheloai'),
                'id' => (int) $request -> input('id'),
                'tenkhongdau' => (string) $request -> input('tenkhongdau'),

            ]);
            Session::flash ('success', 'Tạo Thể Loại Thành Công');

        } catch (\Exception $err) {
            Session::flash('error', $err->getMessage());
            return false;
        }
        return true;
    }

    public function destroy($request)
    {
        $id = (int) $request -> input('id');
        $theloai = theloai::where('id',$request->input('id'))->first();
        if ($theloai){
            return theloai::where('id',$id)->delete();
        }
        return false;
    }
    public function update($request, $theloai) :bool
    {
        $theloai->tentheloai = (string) $request-> input('tentheloai');
        $theloai->tenkhongdau = (string) $request-> input('tenkhongdau');
        $theloai->save();
        Session::flash('success', 'Cập Nhật Thể Loại Thành Công');
        return true;
    }
}