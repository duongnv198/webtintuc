<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\tintuc\tintucService;

class TintucController extends Controller
{
    protected $tintucService;
    public function __construct(tintucService $tintucService)
    {
        $this -> tintucService = $tintucService;
    }


  public function index($id = '', $slug = '')
  {
        $tintuc = $this->tintucService->show($id);
        
        return view('products.content', [
            'title' => $tintuc -> tieude,
            'tintuc' => $tintuc
        ]);
  }
}
