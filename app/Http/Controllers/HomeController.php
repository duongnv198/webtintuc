<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\Menu\MenuService;
use App\Http\Services\tintuc\tintucService;

class HomeController extends Controller
{
    protected $menu;
    protected $tintuc;

    public function __construct(MenuService $menu, tintucService $tintuc)
    {
        $this -> menu = $menu;
        $this -> tintuc = $tintuc;
    }


    public function index()
    {
        return view('main', [
        'title' => 'Web Tin Tuc GIN',
        'menus' => $this -> menu-> show(),
        'tintucs' => $this -> tintuc -> get()
    
    ]);
    }
}
