<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\theloai\CreateFormRequest;
use Illuminate\Http\Request;
use App\Http\Services\theloai\theloaiService;
use App\Models\theloai;
use Illuminate\Support\Facades\DB;


class TheLoaiController extends Controller
{
    protected $theloaiService;
    public function __construct(theloaiService $theloaiService )
    {
        $this -> theloaiService = $theloaiService;
    }
    
    public function create()
    {
        return view('admin.theloai.add', [
            'title' => 'Thêm Thể Loại Mới'
        ]);
    }
    public function store(CreateFormRequest $request)
    {
        $result = $this -> theloaiService->create($request);
        return redirect()-> back();
    }

    public function index()
    {
        $theloais = DB::table('theloai')->select('*');
        $theloais = $theloais->get();
        return view('admin.theloai.list',[
            'title' => 'Danh Sách Thể Loại Mới Nhất',
            'theloais' => $theloais
        ]);
    }
    public function destroy(Request $request)
    {
        $result = $this -> theloaiService->destroy($request);
        if ($request){
            return response()-> json([
                'error' => false,
                'message' => 'Xoá thành công thể loại'
            ]);
        }
        return response()-> json([
            'error' => true
        ]);
    }

    public function show(theloai $theloai)
    {
        return view('admin.theloai.edit',[
            'title' => 'Chỉnh sửa Thể Loại:' . $theloai -> tentheloai,
            'theloai' => $theloai,
            'theloais' => $this->theloaiService->gettheloai1()
        ]);
    }

    public function update(theloai $theloai, CreateFormRequest $request)
    {
        $this -> theloaiService->update($request, $theloai);
        return redirect('/admin/theloai/list');
    }

}

