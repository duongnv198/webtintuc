<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\tintuc\CreateFormRequest;
use Illuminate\Http\Request;
use App\Http\Services\tintuc\tintucService;
use Illuminate\Support\Facades\DB;
use App\Models\tintuc;
use Illuminate\Support\Facades\Session;
use App\Models\theloai;

class TinTucController extends Controller
{
    protected $tintucService;
    public function __construct(tintucService $tintucService )
    {
        $this -> tintucService = $tintucService;
    }
    public function create()
    {
        return view('admin.tintuc.add', [
            'title' => 'Thêm Tin Tức Mới',
            'tintucs' => $this->tintucService->getid_theloai()
        ]);
    }
    public function store(CreateFormRequest $request)
    {
        // dd($request->all());
        $result = $this -> tintucService->create($request);
        return redirect()->route('tintuc.list');
        // return redirect()-> back();
    }
    public function index()
    {
        Session::put('tasks_url',request()->fullUrl());
       /*  $tintucs = DB::table('tintuc')->select('*') ;
        $tintucs = $tintucs->get();
        $tintucs = tintuc::paginate(10); */ 
        $tintucs = DB::table('tintuc')->whereNull('deleted_at')->paginate(10);
        $theloai = $this->tintucService->tenTheLoai();

        return view('admin.tintuc.list',[
            'title' => 'Danh Sách Tin Tức',
            'tintucs' => $tintucs,
            'theloai' => $theloai
        ]); 
    }
    public function destroy(Request $request)
    {
        $result = $this -> tintucService->destroy($request);
        if ($request){
            return response()-> json([
                'error' => false,
                'message' => 'Xoá thành công Tin Tức'
            ]);
        }
        return response()-> json([
            'error' => true
        ]);
    }

    public function show(tintuc $tintuc,theloai $theloai)
    {
        return view('admin.tintuc.edit',[
            // 'title' => 'Chỉnh sửa Tin Tức:' . $tintuc -> tieude,
            'title' => 'Chỉnh sửa Tin Tức',
            'tintuc' => $tintuc,
            'theloai' => $theloai,
            'tintucs' => $this->tintucService->getid_theloai()

        ]);
    }

    public function update(tintuc $tintuc, CreateFormRequest $request)
    {
        // dd($request->all);
        $this -> tintucService->update($request, $tintuc);
        if (session('tasks_url')){
            return redirect(session('tasks_url'));
        }
        return redirect('/admin/tintuc/list');
    }
}
