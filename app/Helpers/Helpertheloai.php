<?php


namespace App\Helpers;


class Helpertheloai
{
    public static function theloai($theloais, $id = 0, $char = '')
    {
        $html = '';

        foreach ($theloais as $key => $theloai) {
            if ($theloai -> id == $id){
                $html  .= '
                <tr>
                    <td>' . $theloai -> id . '</td>
                    <td>' . $char . $theloai -> tentheloai . '</td>
                    <td>'. $theloai -> updated_at . '</td>
                    <td>&nbsp;</td>
                </tr>
                ';

                unset($theloai[$key]);

                $html .= self::theloai($theloai, $theloai -> id);
            }
        }
        return $html;
    }
}