<?php


namespace App\Helpers;

use Illuminate\Support\Str;

class Helper
{
    public static function menu($menus, $parent_id = 0, $char = '')
    {
        $html = '';

        foreach ($menus as $key => $menu) {
            if ($menu -> parent_id == $parent_id){
                $html  .= '
                <tr>
                    <td>' . $menu -> id . '</td>
                    <td>' . $char . $menu -> name . '</td>
                    <td> ' . $menu -> description . ' </td>
                    <td>'. self::active($menu -> active) . '</td>
                    <td>'. $menu -> updated_at . '</td>
                    <td>
                    <a class="btn btn-primary btn-sm" href="/admin/menu/edit/' . $menu->id . '">
                    <i class="fas fa-edit"></i>
                    </a> 

                    <a href="#" class="btn btn-danger btn-sm" 
                        onclick="removeRow(' . $menu->id . ', \'/admin/menu/destroy\')">
                    <i class="fas fa-trash"></i>
                    </a>

                    </td>
                </tr>
                ';

                unset($menus[$key]);

                $html .= self::menu($menus, $menu -> id, $char .'|--');
            }
        }
        return $html;
    }

    public static function active($active = 0) : string
    {
        return $active ==0 ? '<span class="btn btn-danger btn-sm"> NO </span>'
         :'<span class="btn btn-success btn-sm"> YES </span>';
    }

    public static function summary($str, $limit = 200, $strip = false)
    {
        $str = ($strip == true) ? strip_tags($str) : $str;
        if (strlen($str) > $limit) {
            $str = substr($str, 0, $limit - 3);

            return substr($str, 0, strrpos($str, ' ')).'...';
        }

        return trim($str);
    }

    public static function menus($menus, $parent_id = 0) :string
    {
        $html = '';
        foreach ($menus as $key => $menu)
        {
            if ($menu->parent_id == $parent_id)
            {
                $html .= '
                <strong>
                <div class="nav-item dropdown">

                    <a class="nav-link" href="/danh-muc/' . $menu->id . '-' . Str::slug($menu->name, '-') .'.html">    
                    ' . $menu->name . '
                    </a>';
                    unset($menus[$key]);
                    if (self::isChild($menus, $menu->id)){
                        $html .= '<div class="dropdown-menu">';
                        $html .= self::menus($menus, $menu->id);
                        $html .= '</div>';
                    }
                    $html .= '</div>
                    </strong>
                ';
            }
        }
        return $html;
    }


public static function isChild($menus, $id) :bool
{
    foreach ($menus as $menu)
    {
        if ($menu -> parent_id == $id)
        {
            return true;
        }
    }

    return false;  
}
}