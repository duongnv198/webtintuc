<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\tintuc;

class Menu extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'parent_id',
        'description',
        'content',
        'active'
    ];
    public function tintucs()
{
    return $this->hasMany(tintuc::class,'id_theloai', 'id');
}
}
