<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class binhluan extends Model
{
    use HasFactory;
    protected $table = 'binhluan';
    public function tintuc()
    {
        return $this->belongsTo('App\tintuc','id_tintuc','id');
    }
    public function user()
    {
        return $this -> belongsTo('App\User', 'id_User','id');
    }
}
