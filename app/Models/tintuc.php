<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Menu;

class tintuc extends Model
{
    use HasFactory;
    // protected $table = "theloai";
    protected $table = "tintuc";
    protected $fillable = [
        'tieude',
        'tomtat',
        'noidung',
        'id_theloai',
        'noibat',
        'tieudekhongdau',
        'hinh' ,
    ];

public function menu()
{
    return $this->hasOne(Menu::class, 'id', 'menu_id')
    ->withDefault(['name'=>'']);
}
}
