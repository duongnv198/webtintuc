<div class="nav-bar">
    <div class="container">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a href="#" class="navbar-brand">MENU</a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="social ml-auto">
                <a href="/"><i class="fa fa-home"></i></a>
            </div>

            <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                <div class="navbar-nav mr-auto">
                <strong>
                    {{-- <div class="active-menu"><a href="/">  Trang Chủ </a></div> --}}
                    <a href="/" class="nav-item nav-link">Trang Chủ</a>
                </strong>
                    {!! \App\Helpers\Helper::menus($menus) !!}
                
                    {{-- <div class="nav-item dropdown ">
                       <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" >Thời sự</a>

                       <div class="dropdown-menu">
                        <a href="#" class="dropdown-item">Chính trị</a>
                        <a href="#" class="dropdown-item">Dân sinh</a>
                        </div>

                    </div>


                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Góc nhìn</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">Chính trị & chính sách</a>
                            <a href="#" class="dropdown-item">Kinh doanh & quản trị</a>
                        </div>
                    </div>

                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Thế giới</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">Người Việt 5 châu</a>
                            <a href="#" class="dropdown-item">Quân sự</a>
                        </div>
                    </div>

                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Giải trí</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">Giới sao</a>
                            <a href="#" class="dropdown-item">Phim</a>
                        </div>
                    </div>

                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Thể thao</a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">Bóng đá</a>
                            <a href="#" class="dropdown-item">Tennis</a>
                        </div>
                    </div> --}}

                </div>
                <div class="social ml-auto">
                    <a href=""><i class="fab fa-facebook-f"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-youtube"></i></a>
                </div>
            </div>
        </nav>
    </div>
</div>