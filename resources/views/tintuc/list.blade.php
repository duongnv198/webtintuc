<div class="main-news">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    @foreach ($tintucs as $key => $tintuc)
                    <div class="col-md-4">
                        <div class="mn-img">
                            {{-- <img src="/template/img/news-350x223-1.jpg" /> --}}
                            <img src="{{$tintuc->hinh}}" />
                            <div class="mn-title">
                                <a href="/tin-tuc/{{$tintuc->id}}-{{Str::slug($tintuc->tieude, '-')}}.html">
                                    <span> {{$tintuc->tieude}} </span>
                            </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="col-lg-3">
                <div class="mn-list">
                    <h2>Read More</h2>
                    @foreach ($tintucs as $key => $tintuc)
                    <ul>
                        <li><a href="/tin-tuc/{{$tintuc->id}}-{{Str::slug($tintuc->tieude, '-')}}.html">
                            @if ($tintuc->noibat == 0)
                            {{$tintuc->tieude}}
                        @endif
                        </a></li>
                    </ul>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main News End-->
