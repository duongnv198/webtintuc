<div class="panel-footer text-center">
    @if ($paginator->hasPages())
        <ul class="pagination justify-content-center">

            @if (!$paginator->onFirstPage())
                <li><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">«</a></li>
            @endif

            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="disabled"><a class="page-link" href="javascript:void(0)">{{ $element }}</a></li>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @else
                            <li class=""><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <li class=""><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">»</a></li>
            @endif
        </ul>
    @endif
</div>