@extends('admin.main')
@section('content')
        <form action="" method="POST">
            <div class="card-body">
            <div class="row">
                    <div class="col-md-6">
            <div class="form-group">
                <label for="menu">Tiêu Đề</label>
                <input type="text" name="name" class="form-control" value="{{old('name')}}">
            </div>
                    </div>

            {{-- <div class="form-group">
                <label for="theloai">Thể Loại</label>
                <select class="form-controll" name="id">
                    <option value="0"> Thể Loại </option>
                </select>
            </div> --}}
            <div class="col-md-6">
            <div class="form-group">
                <label for="menu">Đường dẫn</label>
                <input type="text" name="url" value="{{old('url')}}" class="form-control">
            </div>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label for="menu"> Ảnh  </label>
            <input type="file" class="form-control" id="upload">
            <div id="image_show">

            </div>
            <input type="hidden" name="hinh" id="file">
        </div>
                <div class="form-group">
                     <label for="menu">Sắp xếp</label>
                     <input type="number" name="sort_by" class="form-control" value="1">
                </div>

            <div class="form-group">
                <label> Kích Hoạt</label>
                <div class="custom-control custom-radio">
                    <input class="custom-control-input" value="1" type="radio" id="active" name="active" checked="">
                    <label for="active" class="custom-control-label"> Có </label>
            </div>

            <div class="custom-control custom-radio">
                <input class="custom-control-input" value="0" type="radio" id="no_active" name="active">
                <label for="no_active" class="custom-control-label">Không</label>

            </div>


            <div class="card-footer">
                <a href="{{ url()->previous() }}" class="btn btn btn-danger">Thoát Ra</a>
                <button type="submit" class="btn btn-success">Tạo Slider</button>
            </div>
            @csrf
        </form>
@endsection