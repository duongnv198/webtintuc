@extends('admin.main')

@section('content')

<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="5%">ID</th>
                <th width="20%">Tiêu Đề</th>
                <th width="20%">LINK</th>
                <th width="10%">Ảnh</th>
                <th width="10%">Trạng Thái</th>
                <th width="10%">Update</th>
                {{-- <th width="5%"> Ảnh </th> --}}
                <th>&nbsp;</th> 
            </tr>
        </thead>
        <tbody>
            @foreach($sliders as $slider)
                <tr>
                    <td>{{$slider -> id}}</td>
                    <td>{{$slider -> name}}</td>
                    <td>{{$slider -> url}}</td>
                    <td>
                        <a href="{{$slider->hinh}}" target="_blank">
                        <img src="{{$slider->hinh}}" height="40px">
                        </a>
                    </td>
                    <td>
                        @if ($slider->active == 1)
                            <span class="btn btn-success btn-sm"> YES </span>
                        @else
                            <span class="btn btn-danger btn-sm"> NO </span>
                        @endif
                    </td>
                    <td>{{$slider -> updated_at}}</td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="/admin/sliders/edit/{{$slider->id}} ">
                            <i class="fas fa-edit"></i>
                        </a> 
                        <a href="#" class="btn btn-danger btn-sm" 
                            onclick="removeRow( {{$slider->id}}, '/admin/sliders/destroy') ">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
</div>

{{-- {{ $tintucs->links() }} --}}
{{ $sliders->links('layout.pagination') }}

{{-- cần thì tự để lại --}}
{{-- <style>
    .w-5{
        display:none chưa bạn ạ, kiểu nó ko đánh dấu xem nó đang đứng ở trang nào nhỉ, thế tự css đc mà, t beiest đâu, toànm làm theo video hướng dẫn
    }
</style> --}}
@endsection
