@extends('admin.main')
@section('content')
        <form action="" method="POST">
            <div class="card-body">
            <div class="row">
                    <div class="col-md-6">
            <div class="form-group">
                <label for="menu">Tiêu Đề</label>
                <input type="text" name="name" class="form-control" value="{{$slider->name}}">
            </div>
                    </div>

            {{-- <div class="form-group">
                <label for="theloai">Thể Loại</label>
                <select class="form-controll" name="id">
                    <option value="0"> Thể Loại </option>
                </select>
            </div> --}}
            <div class="col-md-6">
            <div class="form-group">
                <label for="menu">Đường dẫn</label>
                <input type="text" name="url" value="{{$slider->url}}" class="form-control">
            </div>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label for="menu"> Ảnh  </label>
            <input type="file" class="form-control" id="upload">
            <div id="image_show">
                <a href="{{$slider->hinh}}">
                    <img src="{{$slider->hinh}}" width="100px">
                </a>
            </div>
            <input type="hidden" name="hinh" id="file" value="{{$slider->hinh}}">
        </div>
                <div class="form-group">
                     <label for="menu">Sắp xếp</label>
                     <input type="number" name="sort_by" class="form-control" value="{{$slider->sort_by}}">
                </div>

            <div class="form-group">
                <label> Kích Hoạt</label>
                <div class="custom-control custom-radio">
                    <input class="custom-control-input" value="1" type="radio" id="active" name="active"
                    {{$slider->active == 1 ? 'checked':""}}>
                    <label for="active" class="custom-control-label"> Có </label>
            </div>

            <div class="custom-control custom-radio">
                <input class="custom-control-input" value="0" type="radio" id="no_active" name="active"
                {{$slider->active == 0 ? 'checked':""}}>
                <label for="no_active" class="custom-control-label">Không</label>

            </div>


            <div class="card-footer">
                <a href="{{ url()->previous() }}" class="btn btn btn-danger">Thoát Ra</a>
                <button type="submit" class="btn btn-success">Cập Nhật Slider</button>
            </div>
            @csrf
        </form>
@endsection