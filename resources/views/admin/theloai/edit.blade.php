@extends('admin.main')
@section('content')
        <form action="" method="POST">
            <div class="card-body">

            <div class="form-group">
                <label for="theloai">Tên Thể Loại</label>
                <input type="text" name="tentheloai" value="{{ $theloai->tentheloai }}" class="form-control" placeholder="Nhập Tên Thể Loại">
            </div>

            {{-- <div class="form-group">
                <label for="theloai">Thể Loại</label>
                <select class="form-controll" name="id">
                    <option value="0"> Thể Loại </option>
                </select>
            </div> --}}

            <div class="form-group">
                <label for="theloai">Mô Tả</label>
                <textarea name="tenkhongdau" class="form-control"> {{ $theloai->tenkhongdau }} </textarea>
            </div>
            </div>
            <div class="card-footer">
                <a href="{{ url()->previous() }}" class="btn btn btn-danger">Thoát Ra</a>
                <button type="submit" class="btn btn-success">Cập Nhật Thể Loại</button>
            </div>
            @csrf
        </form>
@endsection