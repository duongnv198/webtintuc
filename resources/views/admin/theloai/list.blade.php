@extends('admin.main')

@section('content')
<table class="table">
  <thead>
      <tr>
          <th style="width: 50px">ID</th>
          <th>Tên Thể Loại</th>
          <th> Mô Tả </th>
          <th>Update</th>
          <th style="width: 100px">&nbsp;</th>
      </tr>
  </thead>
  <tbody>
    @foreach($theloais as $theloai)
      <tr>
          <td>{{$theloai -> id}}</td>
          <td>{{$theloai -> tentheloai	}}</td>
          <td> {{$theloai -> tenkhongdau}} </td>
          <td>{{$theloai -> updated_at}}</td>
          <td>
            <a class="btn btn-primary btn-sm" href="/admin/theloai/edit/{{$theloai->id}} ">
            <i class="fas fa-edit"></i>
            </a> 

            <a href="#" class="btn btn-danger btn-sm" 
               onclick="removeRow({{$theloai->id}} , '/admin/theloai/destroy') ">
            <i class="fas fa-trash"></i>
            </a>

            </td>
      </tr>
      @endforeach
  </tbody>
</table>
@endsection
