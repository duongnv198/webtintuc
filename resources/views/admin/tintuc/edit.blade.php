@extends('admin.main')
@section('head')
<script src="/ckeditor/ckeditor.js"></script>
@endsection
@section('content')
        <form action="" method="POST">
            <div class="card-body">

            <div class="form-group">
                <label>Tên Tin Tức</label>
                <input type="text" name="tieude" value="{{ $tintuc->tieude }}" class="form-control" placeholder="Nhập Tên Tin Tức">
            </div>

            <div class="form-group">

            <div class="form-group">
                <label>Tóm Tắt</label>
                <textarea name="tomtat" id="tomtat" class="form-control"> {{ $tintuc->tomtat }} </textarea>
            </div>

            <div class="form-group">
                <label> Nội Dung</label>
                <textarea name="noidung" id="noidung" class="form-control"> {{ $tintuc->noidung }} </textarea>
            </div>

            <div class="form-group">
                <label>Thể Loại</label>
                <select class="form-controll" name="id_theloai">

                    @foreach($tintucs as $theloai)
                    <option value="{{ $theloai->id}}">{{ $theloai->tentheloai }}</option>
                    
                    @endforeach


                </select>
            </div>

            <div class="form-group">
                <label for="menu"> Ảnh  </label>
                <input type="file" class="form-control" id="upload">
                <div id="image_show">
                    <a href="{{$tintuc->hinh}}">
                        <img src="{{$tintuc->hinh}}" width="100px">
                    </a>
                </div>
                <input type="hidden" name="hinh" id="file" value="{{$tintuc->hinh}}">
            </div>

            <div class="form-group">
                <label> Nổi Bật</label>
                <div class="custom-control custom-radio">
                    <input class="custom-control-input" value="1" type="radio" id="noibat" 
                    name="noibat" {{ $tintuc->noibat == 1 ? 'checked="" ' : '' }}>
                    <label for="noibat" class="custom-control-label"> Có </label>
            </div>

            <div class="custom-control custom-radio">
                <input class="custom-control-input" value="0" type="radio" id="no_noibat" 
                name="noibat" {{ $tintuc->noibat == 0 ? 'checked="" ' : '' }}>
                <label for="no_noibat" class="custom-control-label">Không</label>

            </div>

            </div>
            </div>
            <div class="card-footer">
                <a href="{{ url()->previous() }}" class="btn btn btn-danger">Thoát Ra</a>
                <button type="submit" class="btn btn-success">Cập Nhật Tin Tức</button>
            </div>

            @csrf
        </form>
@endsection
@section('footer')
<script>
    // Replace the <textarea id="editor1"> with a CKEditor 4
    // instance, using default configuration.
    CKEDITOR.replace( 'noidung' );
</script>
@endsection