@extends('admin.main')

@section('content')

<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="5%">ID</th>
                <th width="19%">Tiêu Đề</th>
                <th width="23%">Tóm Tắt</th>
                <th width="27%">Nội dung</th>
                <th width="6%">Nổi Bật</th>
                <th width="5%">Thể Loại</th>
                <th width="5%"> Ảnh </th>
                <th>&nbsp;</th> 
            </tr>
        </thead>
        <tbody>
            @foreach($tintucs as $tintuc)
                <tr>
                    <td>{{$tintuc -> id}}</td>
                    <td>{{$tintuc -> tieude}}</td>
                    <td>
                        {!! \App\Helpers\Helper::summary($tintuc -> tomtat) !!}
                    </td>
                    <td>
                        {!! \App\Helpers\Helper::summary($tintuc -> noidung) !!}                        
                    </td>
                    <td>
                        @if ($tintuc->noibat == 1)
                            <span class="btn btn-success btn-sm"> YES </span>
                        @else
                            <span class="btn btn-danger btn-sm"> NO </span>
                        @endif
                    </td>
                    <td>
                        {{ $theloai[$tintuc->id_theloai] ?? '' }}
                    </td>

                    <td>
                        <a href="{{$tintuc->hinh}}" target="_blank">
                        <img src="{{$tintuc->hinh}}" height="40px">
                        </a>
                    </td>

                    <td>
                        <a class="btn btn-primary btn-sm" href="/admin/tintuc/edit/{{$tintuc->id}} ">
                            <i class="fas fa-edit"></i>
                        </a> 
                        <a href="#" class="btn btn-danger btn-sm" 
                            onclick="removeRow( {{$tintuc->id}}, '/admin/tintuc/destroy') ">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
</div>

{{-- {{ $tintucs->links() }} --}}
{{ $tintucs->links('layout.pagination') }}

{{-- cần thì tự để lại --}}
{{-- <style>
    .w-5{
        display:none chưa bạn ạ, kiểu nó ko đánh dấu xem nó đang đứng ở trang nào nhỉ, thế tự css đc mà, t beiest đâu, toànm làm theo video hướng dẫn
    }
</style> --}}
@endsection
