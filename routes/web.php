<?php

use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\Users\LoginController;
use App\Http\Controllers\Admin\TheLoaiController;
use App\Http\Controllers\Admin\TinTucController;
use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('admin/users/login',[LoginController::class, 'index']) -> name('login');
Route::post('admin/users/login/store', [LoginController::class,'store']);

Route::middleware(['auth'])->group(function(){

    Route::prefix('admin') -> group(function(){
    
    Route::get ('/', [MainController::class, 'index'] ) -> name ('admin'); 
    Route::get ('main', [MainController::class, 'index'] ); 

        #menu
        Route::prefix('menu') -> group(function(){
            Route::get('add',[MenuController::class, 'create']);
            Route::post('add',[MenuController::class, 'store']);
            Route::get('list',[MenuController::class, 'index']);
            Route::get('edit/{menu}',[MenuController::class, 'show']);
            Route::post('edit/{menu}',[MenuController::class, 'update']);
            Route::DELETE('destroy',[MenuController::class, 'destroy']);
        });
        #theloai
        Route::prefix('theloai') -> group(function(){
            Route::get('add',[TheLoaiController::class, 'create']);
            Route::post('add',[TheLoaiController::class, 'store']);
            Route::get('list',[TheLoaiController::class, 'index']);
            Route::get('edit/{theloai}',[TheLoaiController::class, 'show']);
            Route::post('edit/{theloai}',[TheLoaiController::class, 'update']);
            Route::DELETE('destroy',[TheLoaiController::class, 'destroy']);
        });

        #tintuc
        Route::prefix('tintuc') -> group(function(){
            Route::get('add',[TinTucController::class, 'create']);
            Route::post('add',[TinTucController::class, 'store']) -> name('upload.store');
            Route::get('list',[TinTucController::class, 'index'])->name('tintuc.list');
            Route::get('edit/{tintuc}',[TinTucController::class, 'show']);
            Route::post('edit/{tintuc}',[TinTucController::class, 'update']);
            Route::DELETE('destroy',[TinTucController::class, 'destroy']);

        });

        #Slider
        Route::prefix('sliders') -> group(function(){
            Route::get('add',[SliderController::class, 'create']);
            Route::post('add',[SliderController::class, 'store']) -> name('upload.store');
            Route::get('list',[SliderController::class, 'index'])->name('tintuc.list');
            Route::get('edit/{slider}',[SliderController::class, 'show']);
            Route::post('edit/{slider}',[SliderController::class, 'update']);
            Route::DELETE('destroy',[SliderController::class, 'destroy']);

        });

        #Upload
        Route::POST('upload/services',[UploadController::class, 'store']);



});
});


Route::get('/', [HomeController::class, 'index']);

Route::get('danh-muc/{id}-{slug}.html', [MenuController::class, 'get']);
Route::get('tin-tuc/{id}-{slug}.html', [HomeController::class, 'index']);

// Route::get('/', function () {
//     return view('welcome');
// });
